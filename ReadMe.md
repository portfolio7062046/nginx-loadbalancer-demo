# Implementing Round Robin Load Balancing with NGINX, Docker, and a Simple JS App

## Overview

This project demonstrates how to implement and visualize the round-robin load balancing algorithm using NGINX Docker and a simple JavaScript application. The goal is to distribute incoming requests evenly across multiple instances of the JS application using NGINX as a load balancer.

## Features

- **Simple JS App:** A basic JavaScript application that responds to HTTP requests.
- **NGINX Load Balancer:** Configured to use the round-robin algorithm to distribute requests across multiple instances of the JS app.
- **Dockerized Setup:** Docker configurations for the JS app, allowing easy deployment and scaling.

## Getting Started

Follow these steps to set up the project:

### Prerequisites

- [Docker](https://docs.docker.com/get-docker/) installed on your machine.
- [Docker Compose](https://docs.docker.com/compose/install/) installed.
- [Nginx](https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-22-04) installed.

### Clone the Repository


- 'git clone https://gitlab.com/your-username/nginx-load-balancer-js-app.git'
- 'cd nginx-load-balancer-js-app'

### Start the Nginx
- 'systemctl start nginx'

### Build the Docker Image
- 'docker build -t server-app server-app/.'

### Start the containers
- 'docker-compose up -d'

### Copy the nginx.conf file 
- 'cp nginx.conf /etc/nginx/nginx/conf'

## Result
- Once the containers are up and running you can hit the port 8080 to see nginx directing you to different container each time. You can see the conainer id on your browser.

